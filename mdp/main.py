def transition(action, neighbor):
    if action == neighbor:
        return 0.8
    if action[0] * neighbor[0] + action[1] * neighbor[1] == 0 :
        return 0.1
    return 0

def exp_utility(action, neighbor, utilities, wall_states, end_states, i, j, unit_step):
    if [i,j] in end_states:
        return 0

    if [i,j] in wall_states:
        return 0

    if i+neighbor[0] not in range(len(utilities)) or j+neighbor[1] not in range(len(utilities[0])): #For out of grid
        return transition(action, neighbor)*utilities[i][j]

    if [i+neighbor[0], j+neighbor[1]] in wall_states:
        return transition(action, neighbor)*utilities[i][j]

    return transition(action, neighbor)*utilities[i+neighbor[0]][j+neighbor[1]]
    

def value_iteration(grid, wall_states, end_states, unit_step, actions, policy, discount, error_perc):
    print "Grid: ", grid
    print "Wall States: ", wall_states
    print "End States: ", end_states
    print "Unit Step: ", unit_step
    print "Actions: ", actions
    print "Discount: ", discount
    print "Rows#: ", len(grid)
    print "Columns#: ", len(grid[0])
    print "Error Percentage: ", error_perc

    policies = []
    for i in range(len(grid)):
        row = []
        for j in range(len(grid[i])):
            row.append(' ')
        policies.append(row)   

    utilities = []
    for i in range(len(grid)):
        row = []
        for j in range(len(grid[i])):
            row.append(0)
        utilities.append(row)
    iter_utilities = []
    for i in range(len(grid)):
        row = []
        for j in range(len(grid[i])):
            row.append(0)
        iter_utilities.append(row)

    print "Utilities: ", utilities
    print "Iter Utilities: ", iter_utilities
    error = 100000
    iteration = 0
    error_max = (((float(error_perc)/100.0)*(1.0-float(discount)))/float(discount) )
    while error > error_max:
        print "Iteration Number:", iteration            
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                print iter_utilities[i][j], " ",
            print ""
        print ""

        error = 0
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                utilities[i][j] = iter_utilities[i][j]
        
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                max_utility = -100000
                index = 0
                for action in actions:
                    expected_utility = 0
                    for neighbor in [[1,0],[-1,0],[0,1],[0,-1]]:
                        expected_utility += exp_utility(action, neighbor, utilities, wall_states, end_states, i, j, unit_step)

                    if expected_utility > max_utility:
                        max_utility = expected_utility
                        policies[i][j] = policy[index]
                    index = index + 1
                iter_utilities[i][j] = grid[i][j] + discount*max_utility
                if [i,j] in end_states or [i,j] in wall_states :
                    policies[i][j] = '-'
                if grid[i][j] == 0:
                    iter_utilities[i][j] += unit_step
                delta = abs(iter_utilities[i][j]-utilities[i][j])
                curr_error = delta
                if  curr_error >  error:
                    error = curr_error

        iteration += 1

    print "Final Utilities:"            
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            print iter_utilities[i][j], " ",
        print ""
    print ""

    print "Final Policy:"            
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            print policies[i][j], " ",
        print ""
    print ""


def main():
    numbers = map(int, raw_input().split())
    if len(numbers) != 2:
        print "Wrong Input Format"
        return
    else:
        rows = numbers[0]
        columns = numbers[1]
        grid = []
        for i in range(rows):
            grid_inputs = map(float, raw_input().split())
            if len(grid_inputs) != columns:
                print "Wrong Input Format"
            else:
                grid.append(grid_inputs)

        end_wall = map(int, raw_input().split())
        end_states = []
        wall_states = []
        if len(end_wall) != 2:
            print "Wrong Input Format"
            return
        else:
            for i in range(end_wall[0]):
                ends_inputs = map(int, raw_input().split())
                if len(ends_inputs) != 2:
                    print "Wrong Input Format"
                    return
                else:
                    end_states.append(ends_inputs)
            for i in range(end_wall[1]):
                walls_inputs = map(int, raw_input().split())
                if len(walls_inputs) != 2:
                    print "Wrong Input Format"
                    return
                else:
                    wall_states.append(walls_inputs) 

        start_state = map(int, raw_input().split())
        if len(start_state) != 2:
            print "Wrong Input Format"
            return

        unit_step = input()
        actions = [[0, 1], [0, -1], [1, 0], [-1,0]]
        policy = ['R','L','D','U']
        discount = 0.99
        error_perc = 1
        value_iteration(grid, wall_states, end_states, unit_step, actions, policy, discount, error_perc )

main()

